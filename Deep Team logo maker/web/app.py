from flask import Flask, render_template


app = Flask(__name__)


@app.route('/')
@app.route('/home')
def index():
   return render_template('new_index.html')


@app.route('/customise_logo')
def customise_logo():
   return render_template('customise_logo.html')


@app.route('/back')
def back():
   return render_template('index.html')


@app.route('/create_logo')
def create_logo():
   return render_template('create_logo.html')


if __name__ == '__main__':
   app.run(debug=True)
